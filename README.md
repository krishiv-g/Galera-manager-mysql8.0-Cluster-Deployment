<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Galera Cluster Setup Guide</title>
</head>
<body>
    <h1>Installing Galera Manager and Automatically Deploying MySQL 8.0 Galera Cluster in Multiple Hosts</h1>
    <h5>Provided that below is my Setup We will now Install Galera Manager and Deploy Mysql 8.0 Cluster on 3 Nodes</h5>
    <p>&nbsp;</p>
    <pre>
Mysql1 192.168.1.102
Mysql2 192.168.1.103
Mysql3 192.168.1.104
Galera-Manager 192.168.1.105
    </pre>
    <p>Assuming All Servers are Fresh Installed Ubuntu 20.04 Servers</p>
    <pre>
apt update -y
apt install wget -y
wget https://galeracluster.com/galera-manager/gm-installer
chmod +x gm-installer
./gm-installer install
    </pre>
    <p>The gm installer will start an interactive shell and will ask for a few options, please give information accordingly. If you need SSL, please give GDM a domain name while installing instead of an IP address, point the domain to the IP address and it will automatically install SSL certificates using Let's Encrypt.</p>
    <p>Once the installation finishes please go to the IP address of Galera manager i.e for me http://192.168.1.105</p>
    <p>&nbsp;</p>
    <p>On the main screen, click on <strong>Add Cluster</strong>.</p>
    <img src="https://hackmd.io/_uploads/rkudzxA63.png" alt="Add Cluster">
    <p>Click on <strong>Deploy Cluster on user-provided hosts</strong>.</p>
    <p>Give it a name and select the appropriate host OS i.e Ubuntu 20 and click next.</p>
    <img src="https://hackmd.io/_uploads/r1QAfeCph.png" alt="Deploy Cluster">
    <p>The Galera manager needs root access to the host server i.e., all MySQL nodes we shall use so it will present an SSH key that we need to add to the host servers.</p>
    <img src="https://hackmd.io/_uploads/ryYNXx0p3.png" alt="SSH Key">
    <p>Copy that key and paste it inside <code>/root/.ssh/authorized_keys</code>, if the folder <code>/root/.ssh</code> is not present, create it.</p>
    <pre>
mkdir /root/.ssh
nano /root/.ssh/authorized_keys
    </pre>
    <p>Once the key is pasted, restart the SSH service using the command:</p>
    <pre>
service sshd restart
    </pre>
    <p>Click on <em>"I have added the public key"</em> as shown in the picture below and click finish.</p>
    <img src="https://hackmd.io/_uploads/r1_Rml063.png" alt="Finish Setup">
    <p>Click on the cluster just created and then select <strong>Add nodes</strong>.</p>
    <img src="https://hackmd.io/_uploads/r1AENgA62.png" alt="Add Nodes">
    <p>Give Node IP address and Check Access, if Access is fine then we can deploy the cluster, Repeat to add as many nodes as you want.</p>
</body>
</html>
